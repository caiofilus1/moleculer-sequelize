# Moleculer Sequelize
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

![Moleculer logo](https://github.com/moleculerjs/moleculer/raw/master/docs/assets/logo.png)
# Table of contents

| Features                              |
|-------------                          | 
|1. [Introduction](#about)              |
|2. [Configuration](#configuration)     |
|3. [Service-Model](#service-model)     |
|4. [Actions](#actions)                 |
|⠀4.1 [get](#get)                       |
|⠀4.2 [find](#find)                     |
|⠀4.3 [list](#list)                     |
|⠀4.4 [create](#create)                 |
|⠀4.5 [update](#update)                 |
|⠀4.6 [remove](#remove)                 |
|⠀4.7 [insert](#remove)                 |
|⠀4.8 [removeMany](#removeMany)         |
|⠀4.9 [updateMany](#updateMany)         |
|5. [Using ctx](#ctx)                   |
|6. [Relationships](#relationships)     |


# About <a name="About"></a>
# Sequelize plugin for Moleculer.js Framework
If you like to use Sequelize and use models in your backend services, but you like to have relationships between models, this is a plugin for you.
Simplifying and make default action for microservices,
actions with more default parameters and callback functions.
### Configuration  <a name="Configuration"></a>
Create Simple Mixin to use with every Service-Models.
```javascript,
const MoleculerSequelize = require("moleculer-sequelize");
const SequelizeMixin = {
    mixins: [MoleculerSequelize],
    settings: {
        sequelize: new Sequelize( "database_development", "root", "123456",{
            host: "127.0.0.1",
            dialect: "mysql",
        }),
    },
};
module.exports = SequelizeMixin;
```
### Service-Model definition <a name="Service-Model"></a>
```javascript
const Sequelize = require("sequelize");
const SequelizeMixin = require("./sequelize.mixin");

module.exports = { 
    name: "users",
    mixins: [SequelizeMixin],
    model: {
        name: "users",
        define: {
            id: {
                primaryKey: true,
                autoIncrement: true,
                type: Sequelize.INTEGER
            },
            name: Sequelize.STRING,
            email: Sequelize.STRING,
            password: Sequelize.STRING,
        },
        association:{},
        options:{},
    },
};
```

#4 Actions <a id="actions"></a>

This lib generate some defaults actions for CRUD operations.
Remember, this lib works based on mixin, so, if you overwrite default handler in 
action, some lib features will stop to work, if you wanna use this lib use the callbacks functions

##4.1 get  <a id="get"></a>
Used to call one single element from table
### Params:

|Param                  |  Type                     |   default         | description                           |
|-------------          |-------------              |---------          |----                                   |
| [id](#id)             | string, number or array   |         -         | Identifier of row                     |
| [where](#where)       | object or string          |         -         | search query                          |
| [fields](#fields)     |string or array            |         -         | fields returned in response           |
| [exclude](#exclude)   | string or array           |         -         | table fields to exclude in response   |

### Callbacks:
[queryHandler(ctx, params)](#queryHandler)<br/>
[formatResult(ctx, result)](#formatResult)


## 4.2 find
The difference between find and list is find don´t have pagination!
#### Params:
|Param                  |  Type             | default  |  description                        |
|-------------          |-------------      |--------- |-----------                          |
| [limit](#limit)       | number            |     -    | limit of items in response          |
| [sort](#sort)         | string            |     -    | model field to sort list            |
| [search](#search)     | string or array   |     -    | applies "LIKE" operation in field   |
| [where](#where)       | object or string  |     -    | search query                        |
| [fields](#fields)     | string or array   |     -    | table fields returned in response   |
| [exclude](#exclude)   | string or array   |     -    | table fields to exclude in response |
### Callbacks:
[queryHandler(ctx, params)](#queryHandler)<br/>
[formatResult(ctx, result)](#formatResult)

## List  <a id="list"></a>
Used to get a paginated list from a table
#### Params:
|Param                  |  Type                     |   default         | description             |
|-------------          |-------------              |---------          |---------|
| [page](#page)         | number                    |         1         | selected page |
| [pageSize](#pageSize) | number                    |         10        | number of item per page|
| [sort](#sort)         | string                    |         -         | model field to sort list |
| [search](#search)     | string or array           |         -         | applies "LIKE" operation in field |
| [where](#where)       | object or string          |         -         | search query |
| [fields](#fields)     |string or array            |         -         | table fields returned in response |
| [exclude](#exclude)   | string or array           |         -         | table fields to exclude in response|
### Callbacks:
[queryHandler(ctx, params)](#queryHandler)<br/>
[formatResult(ctx, result)](#formatResult)


## create  <a id="create"></a>
Used to create a new row in table
### Params:
Params in create actions is the entity you will create

### Callbacks:
[beforeCreate(ctx)](#beforeCreate)<br/>
[afterCreate(ctx, model)](#afterCreate)<br/>
[formatResult(ctx, result)](#formatResult)

## update  <a id="update"></a>
Used to create a update a row based in id primary key in table
### Params:
|Param                  |  Type             | default   | description            |
|-------------          |-------------      |---------- |---------               |
| [id](#id)             | number            |    X      | Identifier of row      |
Params in create actions is the entity you will create

### Callbacks:
[beforeUpdate(ctx)](#beforeUpdate)<br/>
[afterUpdate(ctx, model)](#afterUpdate)<br/>
[formatResult(ctx, result)](#formatResult)

## insert <a id="insert"></a>
Used To create multiple rows in table
### Params:
|Param                  |  Type             | default   |   description                             |
|-------------          |-------------      |---------- |---------                                  |   
| [id](#id)             | number            | required  |  Identifier of row                        |
| [...entity](#id)      | object            | required  |  elements in model that will be updated   |
Params in update actions is the id and rest is the model attributes


### Callbacks:
[beforeCreate(ctx)](#beforeRemove)<br/>
[afterCreate(ctx, model)](#afterRemove)<br/>
[formatResult(ctx, result)](#formatResult)

## remove <a id="remove"></a>
Used To remove a row in table
### Params:
|Param                  |  Type             | default   |   description                             |
|-------------          |-------------      |---------- |---------                                  |   
| [id](#id)             | number            | required  |  Identifier of row                        |
Params in update actions is the id and rest is the model attributes


### Callbacks:
[queryHandler(ctx, params)](#queryHandler)<br/>
[beforeRemove(ctx)](#beforeRemove)<br/>
[afterRemove(ctx, model)](#afterRemove)<br/>
[formatResult(ctx, result)](#formatResult)

## Callbacks:
####queryHandler(ctx, params) <a id="queryHandler"></a>
Useful to modify and param query or add some validation on action, this callback receive two params,
ctx(the context of action) and normalized query for this action, to change default query just modify
"params" variable or return new query object.

#### formatResult(ctx, result) <a name="formatResult"></a>
Useful to modify the result of action, this lib works in entity based crud, but sometimes we need to return
extra fields or change some name field names. This callback receive two params,
ctx(the context of action) and default result for this action, to change default result just modify
"result" variable or return new result object.

#### beforeCreate(ctx) <a name="beforeCreate"></a>
Useful to validate data before create data in database. This callback receive one param,
ctx(the context of action).

#### afterCreate(ctx) <a name="afterCreate"></a>
Useful to do some action after data created in database. This callback receive one param,
ctx(the context of action).

#### beforeUpdate(ctx, model) <a name="beforeUpdate"></a>
Useful to modify the result of action, this lib works in entity based crud, but sometimes we need to return
extra fields or change some name field names. This callback receive two params,
ctx(the context of action) and default result for this action, to change default result just modify
"result" variable or return new result object.

#### afterUpdate(ctx, model) <a name="afterUpdate"></a>
Useful to modify the result of action, this lib works in entity based crud, but sometimes we need to return
extra fields or change some name field names. This callback receive two params,
ctx(the context of action) and default result for this action, to change default result just modify
"result" variable or return new result object.

#### beforeRemove(ctx, model) <a name="beforeRemove"></a>
Useful to modify the result of action, this lib works in entity based crud, but sometimes we need to return
extra fields or change some name field names. This callback receive two params,
ctx(the context of action) and default result for this action, to change default result just modify
"result" variable or return new result object.

#### afterRemove(ctx, model) <a name="afterRemove"></a>
Useful to modify the result of action, this lib works in entity based crud, but sometimes we need to return
extra fields or change some name field names. This callback receive two params,
ctx(the context of action) and default result for this action, to change default result just modify
"result" variable or return new result object.


# Using ctx <a name="ctx"></a>
You can pass the ctx with params for your function 
#### exemple:
````javascript
    action:{
        create :{
        afterCreate(ctx){
            if(ctx.meta.user.type === "ADMIN"){
                ctx.params.type = ctx.meta.user.type;
            } else{
                throw new MoleculerClientError("ACCESS_DENIED", 401)
            }
        }
    }
}
````

### Relationships  <a name="relationships"></a>
Its possible to create Relationships between tables, just adding association key with name of other model and relationType(hasOne, hasMany, belongsTo, belongsToMany)

```javascript

const Sequelize = require("sequelize");
const SequelizeMixin = require("./sequelize.mixin");

module.exports = {
    name: "companies",
    mixins: [SequelizeMixin],
    model: {
        name: "companies",
        define: {
            id: {
                primaryKey: true,
                autoIncrement: true,
                type: Sequelize.INTEGER
            },
            name: Sequelize.STRING,
            email: Sequelize.STRING,
            password: Sequelize.STRING,
        },
    },
    association:[
        {
            model:"users",
            relationship: "hasMany",
        }
    ]
}

```

```javascript
const Sequelize = require("sequelize");
const SequelizeMixin = require("./sequelize.mixin");
module.exports = {
    name: "users",
    mixins: [SequelizeMixin],
    model: {
        name: "users",
        define: {
            id: {
                primaryKey: true,
                autoIncrement: true,
                type: Sequelize.INTEGER
            },
            name: Sequelize.STRING,
            email: Sequelize.STRING,
            password: Sequelize.STRING,
            companyId: Sequelize.INTEGER,
        },
        association:[
            {
               model:"users",
               relationship: "belongsTo",
               foreignkey: "companyId",
            }
        ],
    }
}

```

And to integrate this to CRUD actions just add 'includes' in service-model action

```javascript
const Sequelize = require("sequelize");
const SequelizeMixin = require("./sequelize.mixin");
module.exports = {
    name: "users",
    mixins: [SequelizeMixin],
    actions:{
        findOne:{
            includes: ['items']
        }
    }
}

```

and Will return

```json
{
  "name": "foo",
  "email": "foo@bar.com",
  "password": "123456",
  "items": [{
    "name": "bar"
  }]
}

```
 


